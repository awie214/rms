<?php

/**
 * Public Routes
 *
 */
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/menu', 'HomeController@index')->name('menu');
Route::get('/careers', 'HomeController@careers')->name('careers');
Auth::routes();

/**
 * Routes that require user to be logged in
 * filtered by middleware auth in controller
 */
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/non-plantilla', 'JobsController@nonPlantilla')->name('jobs.nonplantilla');
Route::get('evaluation/rating', 'EvaluationController@rating')->name('evaluation.rating');
Route::get('jobs/publish', 'JobsController@publish')->name('jobs.publish');
Route::get('evaluation/matrix-qualification', 'EvaluationController@matrixQualification')->name('evaluation.matrix');
Route::get('evaluation/comparative-ranking', 'EvaluationController@comparativeRanking')->name('evaluation.comparative');
Route::get('evaluation/report', 'EvaluationController@evaluationReport')->name('evaluation.report');

Route::post('evaluation/store-matrix-qualification',
    'EvaluationController@storeMatrixQualification')->name('evaluation.storeMatrix');

Route::post('evaluation/store-comparative-ranking',
    'EvaluationController@storeComparativeRanking')->name('evaluation.storeComparative');

Route::resources([
    'applicant' => 'ApplicantController',
    'config' => 'ConfigController',
    'evaluation' => 'EvaluationController',
    'jobs' => 'JobsController',
    'recommendation' => 'RecommendationController',
    'appointment' => 'AppointmentController',
    'joboffer' => 'JobOfferController',
    'assumption' => 'AssumptionController',
    'attestation' => 'AttestationController',
    'report' => 'ReportController',
]);


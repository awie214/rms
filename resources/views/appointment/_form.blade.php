@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}

<div class="form-group row">
    {{ Form::label('appointing_officer', 'Appointing Officer/Authority', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-5">
        {{ Form::text('appointing_officer', '', [
                'class' => 'form-control',
                'placeholder' => 'Appointing Officer/Authority'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-2 col-form-label text-sm-right"> Date of Signing </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="signing_date"
                   class="form-control form-control-sm"
                   placeholder="Date of Signing"
                    >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('authorized_official', 'Authorized Official', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-5">
        {{ Form::text('authorized_official', '', [
                'class' => 'form-control',
                'placeholder' => 'Authorized Official'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-2 col-form-label text-sm-right"> Date </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
                   class="form-control form-control-sm"
                   placeholder="Evaluation Made as of"
                >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
      });
    </script>
@endsection

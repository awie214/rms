@section('css')
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'config-form']) !!}
    <div class="form-group row">
        {{ Form::label('name', 'Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('name', $config->name, [
                    'class' => 'form-control',
                    'placeholder' => 'name',
                    'required' => true,
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('value', 'Value', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('value', $config->value, [
                    'class' => 'form-control',
                    'placeholder' => 'value',
                    'required' => true,
                ])
            }}
        </div>
    </div>

    {{ Form::submit('Submit', ['class'=>'btn btn-primary']) }}
{!! Form::close() !!}

@section('scripts')
<!-- frontend validation -->
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script>
    $('#config-form').parsley();
</script>
@endsection

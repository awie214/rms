<!-- JS Libraries -->
<script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-booking.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/fuelux/js/wizard.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wizard.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>

<script>
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    App.wizard();

    // run the parsely validation before changing forms
    $('.wizard-next, .wizard-previous').click(function() {
      tabs.validate(this, 8);
    });

    let tabs = {
      validate: function(element, noOfTabs) {
        // validate each element in current form tab
        var noError = true;
        var result = true;
        $(element).closest('[data-step]').find('input, textarea, select').each(function() {
          $(this).parsley().validate();
          result = $(this).parsley().isValid();
          if (!result) {
            noError = result;
          }
        });

        // set css class active and complete tabs
        let step = $(element).closest('[data-step]').data('step');
        if (!noError) {
          for (i = 1; i <= noOfTabs; i++) {
            if (i != step) {
              $(`[data-step*="${i}"]`).removeClass('active complete');
            }
            else {
              $(`[data-step*="${i}"]`).addClass('active complete');
            }
          }
        }
      },
    };

    let vocational_ctr = 0;
    $('#add_vocational').click(function() {
      vocational_ctr += 1;
      let vocationalHtml = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-3">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][attendance_from]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][attendance_to]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][level]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][graduated]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][awards]" type="text">` +
        `</div>` +
        `</div>`;

      $(vocationalHtml).insertAfter('.vocational');
    });

    let college_ctr = 0;
    $('#add_college').click(function() {
      college_ctr += 1;
      let collegeHTML = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-3">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][attendance_from]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][attendance_to]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][level]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][graduated]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][awards]" type="text">` +
        `</div>` +
        `</div>`;

      $(collegeHTML).insertAfter('.college');
    });

    let graduate_studies_ctr = 0;
    $('#add_graduate_studies').click(function() {
      graduate_studies_ctr += 1;
      let graduateStudiesHTML = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-3">` +
        `<input class="form-control form-control-sm" name="graduate_studies[${graduate_studies_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="graduate_studies[${graduate_studies_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="graduate_studies[${graduate_studies_ctr}][attendance_from]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="graduate_studies[${graduate_studies_ctr}][attendance_to]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="graduate_studies[${graduate_studies_ctr}][level]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="graduate_studies[${graduate_studies_ctr}][graduated]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="graduate_studies[${graduate_studies_ctr}][awards]" type="text">` +
        `</div>` +
        `</div>`;

      $(graduateStudiesHTML).insertAfter('.graduate-studies');
    });

  });
</script>
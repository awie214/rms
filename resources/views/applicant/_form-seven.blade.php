{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
    <div class="col-2 text-center">INCLUSIVE DATES (mm/dd/yyyy)</div>
    <div class="col-3 text-center">POSITION TITLE</div>
    <div class="col-3 text-center">DEPARTMENT/AGENCY/OFFICE/COMPANY</div>
    <div class="col-1 text-center">MONTHLY SALARY</div>
    <div class="col-1 text-center"><small><strong>SALARY/JOB/PAY GRADE(If Applicable) & STEP(Format '00-0')/INCREMENT</strong></small></div>
    <div class="col-1 text-center">STATUS OF APPOINTMENT</div>
    <div class="col-1 text-center">GOV'T SERVICE (Y/N)</div>
</div>
<div class="form-group row">
    <div class="col-1 text-center">
        FROM
        {{ Form::text('work_experience[1][inclusive_date_from]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        TO
        {{ Form::text('work_experience[1][inclusive_date_to]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        (Write in full/Do not abbreviate)
        {{ Form::text('work_experience[1][position_title]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        (Write in full/Do not abbreviate)
        {{ Form::text('work_experience[1][department_company]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][department_company]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        {{ Form::text('work_experience[1][monthly_salary]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        {{ Form::text('work_experience[1][job_grade]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][job_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        {{ Form::text('work_experience[1][appointment_status]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][appointment_status]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        {{ Form::text('work_experience[1][government_service]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][government_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

{{--2nd Row--}}
<div class="form-group row">
    <div class="col-1 text-center">
        {{ Form::text('work_experience[2][inclusive_date_from]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[2][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[2][inclusive_date_to]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[2][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        {{ Form::text('work_experience[2][position_title]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[2][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        {{ Form::text('work_experience[2][department_company]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[2][department_company]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[2][monthly_salary]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[2][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[2][job_grade]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[2][job_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[2][appointment_status]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[2][appointment_status]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[2][government_service]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[2][government_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

{{--3rd Row--}}
<div class="form-group row">
    <div class="col-1 text-center">
        {{ Form::text('work_experience[3][inclusive_date_from]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[3][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[3][inclusive_date_to]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[3][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        {{ Form::text('work_experience[3][position_title]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[3][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        {{ Form::text('work_experience[3][department_company]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[3][department_company]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[3][monthly_salary]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[3][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[3][job_grade]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[3][job_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[3][appointment_status]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[3][appointment_status]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        {{ Form::text('work_experience[3][government_service]', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[3][government_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row text-right">
    <div class="col-12">
        {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
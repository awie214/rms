<script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    App.init();
    App.dataTables();

    $('.publish-link').change(function() {
      var job = $(this);
      let jobID = job.data('id');

      $.ajax({
        url: 'jobs/publish?id=' + jobID,
        type: 'get',
        data: {
          _method: 'get',
          data: {
            '_token': '{{ csrf_token() }}'
          },
        },
        success: function(result) {
          if (result) {
            //job.attr('checked', 'checked');
          }
        },
      });
    });
  });
</script>
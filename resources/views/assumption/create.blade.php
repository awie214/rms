@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Assumption</h2>
    </div>

    <!-- Assumption Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an assumption in the form below.</span></div>
                <div class="card-body">
                    @include('assumption._form', [
                        'action' => $action,
                        'method' => 'POST',
                        'applicant' => $applicant,
                        'assumption' => $assumption
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection

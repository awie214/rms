@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}

<div class="form-group row">
    {{ Form::label('title', 'Position for Consideration', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('reference_no', $applicant->job->title, [
                'class' => 'form-control',
                'placeholder' => 'Reference No.',
                'readonly' => ($applicant->reference_no) ? true : false
            ])
        }}
    </div>

    {{ Form::label('division', 'Division/Office', ['class'=>'col-1 col-form-label']) }}
    <div class="col-3">
        {{ Form::text('division', $applicant->job->division, [
                'class' => 'form-control',
                'placeholder' => 'Division/Office',
                'readonly' => ($applicant->reference_no) ? true : false
            ])
        }}
    </div>

    <div class="col-2 text-right">
        <a href="#" data-modal="md-flipH" class="btn btn-secondary md-trigger"><i
                    class="icon icon-left mdi mdi-print"></i>
            Certification
        </a>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-7">
        <div id="education"></div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-7">
        <div id="experience"></div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-7">
        <div id="eligibility"></div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-7">
        <div id="training"></div>
    </div>
</div>
{!! Form::close() !!}

<div class="main-content container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-table">
                <div class="card-header">
                    <div class="tools dropdown">
                        <span class="icon mdi mdi-download"></span><a href="#" role="button" data-toggle="dropdown"
                                                                      class="dropdown-toggle"><span
                                    class="icon mdi mdi-more-vert"></span></a>
                        <div role="menu" class="dropdown-menu">
                            <a href="#" class="dropdown-item">Action</a><a href="#" class="dropdown-item">Another
                                action</a><a href="#" class="dropdown-item">Something else here</a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">Separated link</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                        <thead>
                        <tr>
                            <th>Name of Applicant & Current Position</th>
                            <th>Age</th>
                            <th>Educational Background</th>
                            <th>Work Experience</th>
                            <th>Training</th>
                            <th>Eligibility</th>
                            <th>Remarks</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $applicant->getFullname() }} <br /> {{ $applicant->job->title }}</td>
                            <td>{{ \Carbon\Carbon::today()->diffInYears($applicant->birthday) }}</td>
                            <td>{!! $applicant->job->education !!}</td>
                            <td>{!! $applicant->job->experience !!}</td>
                            <td>{!! $applicant->job->training !!}</td>
                            <td>{!! $applicant->job->eligibility !!}</td>
                            <td>{{ $applicant->remarks }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('recommendation._form-certificate')

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <!-- wysiwyg -->
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}"
            type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();

        // instansiate summernote(wysiwyg) editor
        App.textEditors('#education', '#experience', '#eligibility', '#training');

        // place data into summernote(wysiwyg) editor
        $('#education').summernote('code', `{!! $applicant->job->education !!}`);
        $('#experience').summernote('code', `{!! $applicant->job->experience !!}`);
        $('#eligibility').summernote('code', `{!! $applicant->job->eligibility !!}`);
        $('#training').summernote('code', `{!! $applicant->job->training !!}`);

        // readonly summernote(wysiwyg) editor
        $('#education').summernote('disable');
        $('#experience').summernote('disable');
        $('#eligibility').summernote('disable');
        $('#training').summernote('disable');

        // remove tools on summernote(wysiwyg) editor
        $('.note-toolbar').remove();

        $.fn.niftyModal('setDefaults', {
          overlaySelector: '.modal-overlay',
          contentSelector: '.modal-content',
          closeSelector: '.modal-close',
          classAddAfterOpen: 'modal-show',
        });

        // print modal
        $('#print-button').click(function() {
          printElement(document.getElementById('printThis'));
        })

        function printElement(elem) {
          const domClone = elem.cloneNode(true);
          const printSection = document.createElement('div');
          printSection.id = 'printSection';
          printSection.appendChild(domClone);
          $('body').append(printSection);
          $('.be-wrapper, .modal-buttons').hide();
          window.print();
          $('.be-wrapper, .modal-buttons').show();
          $('#printSection').remove();
        }

      });
    </script>
@endsection

<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Assumption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AssumptionController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Assumption');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $assumptions = Assumption::with([
                'applicant.job' => function ($query) {
                    $query->where('status', '=', 'plantilla');
                }
            ]
        )->paginate($perPage);

        return view('assumption.index', [
            'assumptions' => $assumptions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $assumption = new Assumption();
        $applicant = new Applicant();

        if ($request->reference) {
            $applicant = Applicant::where('reference_no', $request->reference)
                ->first();
        }

        return view('assumption.create')->with([
            'assumption' => $assumption,
            'applicant' => $applicant,
            'action' => 'AssumptionController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function show(Assumption $assumption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function edit(Assumption $assumption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assumption $assumption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assumption $assumption)
    {
        //
    }
}
